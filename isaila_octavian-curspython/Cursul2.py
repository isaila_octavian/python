# x=10
# while x>1:
#     print("x este", x)
#     x=x-1
#
# euro=input("valoarea euro pentru convertire:")
# if euro.isdigit():
#     euro=int(euro)
#     print("valoarea convertita este:",euro*4.5,"ron")
# else:
#     print("valoarea nu este un numar!")
#
# quit=input("apasa q pentru a iesi si orice tasta pentru a repeta conversia:")
#
# while True:
#     if quit.upper()=="Q":
#         break
#     else:
#         pass
#
# x=10
# while x>1:
#     print("x este {}".format(x))
#     continue
#     x-=1
#
# for i in range(0,50,5):
#     print(i)
#
# for var in "abcd":
#     print (var)
#
# exemplu="abcde"
# print(exemplu[1])
#
# #exemplu[2]="z" nu se poate
# print(exemplu)
# print(exemplu.replace("e","z"))
# print((exemplu.find("ab")))
#
# a=1
# b=2
# s=a+b
# print("suma lui {} +{} este {}".format(a,b,s))
#
# y="sir de caractere"
# print(y.split(" "))
#
# inventar=None
# if not inventar:
#     print("momentan nu ai niciun inventar")
# inventar=("paine","rosii","branza")
# print("\ntuplul inventar este acum {}\n".format(inventar))
#
# for item,value in enumerate(inventar):
#     print("produs {}=>{}".format(item,value))
#     print("\n\n in total avem {} produse".format(len(inventar)))
# input("\n\n apasa <enter> pt a iesi.")
#
# l=[1,2,3,"Google",[3,4,5]]
# l[0]="salut"
# print(l)
# l1=[5,2,9,1]
# l1.sort()
# print(sorted(l1))
# #doar face o copie a liste si o ordoneaza
#
# l=[1,2,3,4,"octa",[1,6,8]]
# print(l[::2])
# #tipareste din 2 in 2
#
# s={1,2,3,4,5}
# print("tipul este {}".format(type(s)))
# x=set()
# x.add(1)
# x.add(2)
# x.add(3)
# print("tipul este {}".format(type(s)))
#
# s1={1,2,3,4,5}
# s2={3,4,5,6,7,8}
# print(s1 & s2)
# print(s1 | s2)
# print(s1-s2)
# print(s1^s2)
# s2.discard(8)
# print(s2)
#
# d={1:"octavian", 2:"tigarean", "numar":1,"varsta":21}
# print(d["varsta"])
# print(d.get(2,""))
# print(d.keys())
# print(d.values())
# print(d.items())
# #mai este pop() si del()

